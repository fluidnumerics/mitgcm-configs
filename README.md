#  Topographic Dynamics of the Gulf Stream/MITgcm-configs

This project is funded by NSF Grant #1829856 (Physical Oceanography Program).

This repository is used to version control the MITgcm input-decks used in support of the [Topographic Dynamics of the Gulf Stream](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1829856&HistoricalAwards=false) project.


For all of our work, we are using [MITgcm checkpoint 67p](https://github.com/MITgcm/MITgcm/releases/tag/checkpoint67p).

## Project description
As a part of the study of the Topographic Dynamics of the Gulf Stream, we are conducting a series of downscaling experiments with the [MIT general circulation model](https://github.com/MITgcm/MITgcm). We are starting at a resolution of 3km (1/36 degree) using boundary conditions derived from the 10km MITgcm simulation described in [this study](http://dx.doi.org/10.1175/JPO-D-15-0133.1). This simulation will be used to derive boundary and initial conditions for a 1km resolution simulation focusing in on the eastern seaboard of the U.S. In turn, this 1km simulation will provide boundary and initial conditions for 300m resolution simulations focusing in on the Charleston Bump and Cape Hatteras, NC. 

The goals of these experiments are to better understand the dynamics of the Gulf Stream as it separates from the continental shelf and heads offshore. [Previous terraforming experiments](https://journals.ametsoc.org/doi/10.1175/JPO-D-16-0195.1?mobileUi=0) and analysis have suggested that topographic wave arrest dynamics are involved in the Gulf Stream separation process.

## Directory structure
Each directory provides the input-decks for building and running the MITgcm experiments associated with this study. The directory names are denoted as follows :

* `res_3km` : 3km Resolution input-decks

Each directory is organized similarly to the directories in the [MITgcm verification examples](https://github.com/MITgcm/MITgcm/tree/master/verification) with a `build/`, `code/`, and `input/` subdirectory.

### Who do I talk to? ###

* joe@fluidnumerics.com (Repository Admin)
