#!/bin/bash

#module purge
#module load gnu8 mvapich2 netcdf netcdf-fortran

spack load gcc@8.2.0 openmpi netcdf-fortran ^openmpi
#spack load gcc@8.2.0 mpich netcdf-fortran ^mpich


make clean
make CLEAN
#../../tools/genmake2 --mods=../code --optfile=fluid_slurm_gcp-gnu_mpich
../../tools/genmake2 --mods=../code --optfile=comedians-gnu_openmpi
#../../tools/genmake2 --mods=../code --optfile=comedians-gnu_mpich
make depend
make
cp mitgcmuv ../run

module purge
