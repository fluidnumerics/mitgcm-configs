#!/bin/bash

module purge
module load gnu8 mvapich2 netcdf netcdf-fortran
make clean
make CLEAN
../../tools/genmake2 --mods=../code --optfile=fluid_slurm_gcp-gnu_mpich
make depend
make
cp mitgcmuv ../run

module purge
