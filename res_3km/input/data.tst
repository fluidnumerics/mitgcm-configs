# ====================
# | Model parameters |
# ====================
#
# Continuous equation parameters
 &PARM01
 viscAh=20.0,
 viscAz=1.E-5,
# Biharmonic diffusivity 
# viscA4=250.0,
 bottomDragLinear=0.E-4,
 bottomDragQuadratic=2.0E-3,
 no_slip_sides=.FALSE.,
 no_slip_bottom=.TRUE.,
 diffKhT=20.0,
 diffKzT=1.0E-5,
# diffK4T=250.0,
 diffKhS=20.0,
# diffK4S=250.0,
 diffKzS=1.0E-5,
 implicitViscosity=.TRUE.,
 implicitDiffusion=.TRUE.,
# beta=1.E-11,
 eosType='MDJWF',
 rigidLid=.FALSE.,
 implicitFreeSurface=.TRUE.,
 hFacMin=0.4,
 nonHydrostatic=.FALSE.,
 readBinaryPrec=32,
 useSingleCPUIO=.TRUE.,
 staggerTimeStep=.TRUE.,
 balanceEmPmR=.TRUE.,
 saltAdvScheme=33,
 tempAdvScheme=33,
 &end
# Elliptic solver parameters
 &PARM02
 cg2dMaxIters=1000,
 cg2dTargetResidual=1.E-13,
 cg3dMaxIters=20,
 cg3dTargetResidual=1.E-8,
 &end
# Time stepping parameters
 &PARM03
 nIter0=10,
 nTimeSteps=10,
 deltaT=180.0,
 abEps=0.01,
 pChkptFreq=1800.0, 
 chkptFreq=1800.0,
 dumpFreq=1800.0,
 monitorSelect=2,
 monitorFreq=720.0,
 periodicExternalForcing=.TRUE.,
 externForcingPeriod=86400.0,
 externForcingCycle=31622400.0,
# tauThetaClimRelax=864000.0,
 &end
# Gridding parameters
 &PARM04
 usingCartesianGrid=.FALSE.,
 usingSphericalPolarGrid=.TRUE.,
 ygOrigin=23.0490,
 dXspacing=0.0333333,
 dYspacing=0.0277778,
 delRFile='dz.box',
 &end
# Input datasets
 &PARM05
 bathyFile='newtopog.box',
 hydrogThetaFile='t-pickup.box',
 pSurfInitFile='e-pickup.box',
 hydrogSaltFile='s-pickup.box',
 uVelInitFile='u-pickup.box',
 vVelInitFile='v-pickup.box',
 &end
