#!/usr/bin/python3
#
#

import os
import subprocess
import shlex
import glob
import shutil

ARCHIVE_PATH='/tank/topog/gulf-stream/simulation/mitgcm_3km_z68/production/'
COMPLETE_NC=['(31 currently)','(30 currently)']

def gluemnc(file_prefix):

    cwd = os.getcwd()
    print(cwd)

    # State files
    if file_prefix == 'grid':
        ncFiles = glob.glob('{}.t001.nc'.format(file_prefix))
    else:
        ncFiles = glob.glob('{}.*.t001.nc'.format(file_prefix))

    glueFiles = True
    for ncFile in ncFiles :

        if file_prefix == 'grid':
            toGlue = '.'.join(ncFile.split('.')[0:1])+'.t*.nc'
            toArchive = '.'.join(ncFile.split('.')[0:1])+'.glob.nc'
        else:
            toGlue = '.'.join(ncFile.split('.')[0:2])+'.t*.nc'
            toArchive = '.'.join(ncFile.split('.')[0:2])+'.glob.nc'

            try:
                print(shlex.split('ncdump -h {}'.format(ncFile)))
                proc = subprocess.Popen(shlex.split('ncdump -h ./{}'.format(ncFile)),
                                                                                    stdout = subprocess.PIPE,
                                                                                    stderr = subprocess.PIPE)
                out, err = proc.communicate()
                for line in out.decode('utf-8').split('\n'):
                    if 'T = UNLIMITED' in line:
                        state = line.split(' // ')[-1]
                        glueFiles = True
                        print(state)
    
                if not state in COMPLETE_NC:
                    print('{} does not have all timelevels yet.'.format(ncFile))
                    glueFiles = False
    
            except subprocess.CalledProcessError as err:
                glueFiles = False
                print(err)

        if glueFiles :
            print('./gluemnc {}'.format(toGlue))
            subprocess.check_call('bash ./gluemnc {}'.format(toGlue),shell=True)

            print('Cleaning up {}...'.format(toGlue))
            subprocess.check_call('rm {}'.format(toGlue),shell=True)

            print('Migrating {} to {}...'.format(toArchive,ARCHIVE_PATH))
            shutil.move(toArchive, '{}/{}'.format(ARCHIVE_PATH,toArchive))

            print('Done!')

   # os.chdir(cwd) 

def glue_netcdf_files():

    #gluemnc('grid')
    gluemnc('state')
    gluemnc('phiHydLow')
    gluemnc('phiHyd')

def main():

    glue_netcdf_files()


if __name__ == '__main__':

    main()
