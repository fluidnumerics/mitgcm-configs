
## Getting Started

```
$ python3 -m venv env
$ source env/bin/activate
$ pip3 install -r requirements.txt
```

## About

### bathymetry-prep.py
Create interpolated bathymetry fields using bathymetry specified in the bathymetry field of the input JSON.
Sample bathymetry field
```
{
    "bathymetry":[
        {
	  "name": "GEBCO Bathymetry",
	  "io_type": "netcdf",
	  "box_config": {},
	  "nc_config" : {"file": "./inputs/gebco.nc","lon_units": "neg_deg_east", "lat_var": "lat", "lon_var": "lon", "topog_var": "elevation", "swap_dimensions": false},
	  "output":"./outputs/gebco_topog.box",
	  "smoothing": {"active": false, "sigma": [12.0, 12.0]},
	  "interp_kind": "cubic"
        },
        {
	  "name": "GEBCO Bathymetry (smoothed)",
	  "io_type": "netcdf",
	  "box_config": {},
	  "nc_config" : {"file": "./inputs/gebco.nc","lon_units": "neg_deg_east", "lat_var": "lat", "lon_var": "lon", "topog_var": "elevation", "swap_dimensions": false},
	  "output":"./outputs/gebco_smoothed_topog.box",
	  "smoothing": {"active": true, "sigma": [12.0, 12.0]},
	  "interp_kind": "cubic"
        }
    ],
}
```
See [mitgcm_3km_z68.json](./mitgcm_3km_z68.json) for example input.
