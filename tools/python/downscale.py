#!/usr/bin/python3
# 
# Author : Joseph Schoonover (joe@fluidnumerics.com)
#
# Supported by NSF Award #1829856 "Topographic Dynamics of the Gulf Stream"
#

import sys
import json
import scipy.interpolate as interp
import scipy.ndimage.filters as filters
import numpy as np
import matplotlib.pyplot as plt
import netCDF4 as nc
import struct

import multiprocessing
from joblib import Parallel, delayed


def load_config():

    with open('input.json') as json_file:
        config = json.load(json_file)

    return config;

#END load_config

def load_mitgcm_grid(config):

    box_config = config['source_grid']['box_config']

    if 'topog_file' in box_config:
        f = open(box_config['topog_file'],'rb')
        topog = np.fromfile(f, '>f4')
        topog.shape = (box_config['shape'][1],box_config['shape'][0])
        f.close()
    else:
        topog = np.zeros((box_config['shape'][1],box_config['shape'][0]))

    f = open(box_config['lat_file'],'rb')
    lat = np.fromfile(f, '>f4')
    lat.shape = (box_config['shape'][1],1)
    f.close()

    f = open(box_config['lon_file'],'rb')
    lon = np.fromfile(f, '>f4')
    lon.shape = (box_config['shape'][0],1)
    f.close()

    nc_file = config['source_grid']['nc_file']
    nc_fid = nc.Dataset(nc_file, 'r')
    nx = nc_fid.dimensions['X'].size
    ny = nc_fid.dimensions['Y'].size
    nz = nc_fid.dimensions['Z'].size

    grid = {'HFacS' : nc_fid.variables['HFacS'][:,:,:],
            'HFacW' : nc_fid.variables['HFacW'][:,:,:],
            'HFacC' : nc_fid.variables['HFacC'][:,:,:],
            'lat' : lat,
            'lon' : lon,
            'topog' : topog,
            'Z' : nc_fid.variables['Z'][:]}

    grid['HFacS'].shape = (nz,ny,nx)
    grid['HFacW'].shape = (nz,ny,nx)
    grid['HFacC'].shape = (nz,ny,nx)
    nc_fid.close()


    return grid

#END load_mitgcm_grid

def load_cheapaml(config, grid):

    cheapaml = []
    for cf in config['cheapaml']['files']:

        f = open(cf,'rb')
        obj = {"file":cf,
               "data":np.fromfile(f, '>f4')}
        obj["data"].shape = (config['cheapaml']['n_timelevels'], 
                             grid['ny'], grid['nx'])
        cheapaml.append(obj)
        f.close()

    return cheapaml

#END load_cheapaml

def inpaint_fillvalues(var, hfac):

    # Masked values will be replaced with inpainted cells that are 
    # found from running a smoothing operator over the grid where
    # the "wet" grid points are clamped, and "dry" grid-points are updated.
    temp = var
    # Reset fill values to 0
    for k in range(var.shape[0]):
        for j in range(var.shape[1]):
            for i in range(var.shape[2]-1,-1,-1):

                if(hfac[k,j,i] == 0.0):
                    temp[k,j,i] = tlast
                else:
                    tlast = var[k,j,i]
    return temp

#END inpaint_fillvalues

def load_mitgcm_state(nc_file, time_level):

    nc_fid = nc.Dataset(nc_file, 'r')

    nx = nc_fid.dimensions['X'].size
    ny = nc_fid.dimensions['Y'].size
    nz = nc_fid.dimensions['Z'].size
    state = {'salt' : nc_fid.variables['S'][time_level,:,:,:],
             'temp' : nc_fid.variables['Temp'][time_level,:,:,:],
             'u' : nc_fid.variables['U'][time_level,:,:,:],
             'v' : nc_fid.variables['V'][time_level,:,:,:],
             'w' : nc_fid.variables['W'][time_level,:,:,:],
             'eta' : nc_fid.variables['Eta'][time_level,:,:]}
    state['salt'].shape = (nz,ny,nx)
    state['temp'].shape = (nz,ny,nx)
    state['u'].shape = (nz,ny,nx)
    state['v'].shape = (nz,ny,nx)
    state['w'].shape = (nz,ny,nx)
    state['eta'].shape = (ny,nx)
    nc_fid.close()

    return state

#END load_mitgcm_state

def generate_target_grid(config):

    x = np.arange(config['target_grid']['lon_range'][0],
                  config['target_grid']['lon_range'][1],
                  config['target_grid']['lon_resolution'])

    y = np.arange(config['target_grid']['lat_range'][0],
                  config['target_grid']['lat_range'][1],
                  config['target_grid']['lat_resolution'])

    return x, y

#END generate_target_grid

def load_bathy(config):

    bathy_cache = []
    for bathymetry in config['bathymetry']:

        if bathymetry['io_type'] == 'box':
            print('Bathymetry IO Type : {}'.format(bathymetry['io_type']))
            x, y, topog = load_box_files(bathymetry['box_config'])
            bathy_cache.append({'x':x,'y':y,'topog':topog,
                                'interp_kind':bathymetry['interp_kind'],
                                'output':bathymetry['output'],
                                'smoothing': bathymetry['smoothing']})
            print('  Bathymetry Dataset : {}'.format(bathymetry['name']))
            print('  Longitude shape : {}'.format(x.shape))
            print('  Latitude shape : {}'.format(y.shape))
            print('  Topog shape : {}'.format(topog.shape))
            

        elif bathymetry['io_type'] == 'netcdf':
            print('Bathymetry IO Type : {}'.format(bathymetry['io_type']))
            x, y, topog = load_nc_files(bathymetry['nc_config'])
            bathy_cache.append({'x':x,'y':y,'topog':topog,
                                'interp_kind':bathymetry['interp_kind'],
                                'output':bathymetry['output'],
                                'smoothing': bathymetry['smoothing']})
            print('  Bathymetry Dataset : {}'.format(bathymetry['name']))
            print('  Longitude shape : {}'.format(x.shape))
            print('  Latitude shape : {}'.format(y.shape))
            print('  Topog shape : {}'.format(topog.shape))

        else:
            print('Unknown Bathymetry IO Type {}. Skipping'.format(bathymetry['io_type']))

    return bathy_cache

#END load_bathy

def smooth_bathy(bathy):

    bathy_cache = []
    for b in bathy:

        if b['smoothing']['active']:

            sigma = b['smoothing']['sigma']
            topog = filters.gaussian_filter(b['topog'], sigma, mode='constant')

            bathy_cache.append({'x':b['x'],'y':b['y'],'topog':topog,
                                'interp_kind':b['interp_kind'],
                                'output':b['output'],
                                'smoothing': b['smoothing']})

        else:
            bathy_cache.append(b)

    return bathy_cache

#end smooth_bathy

def interpolate_bathy(bathy, newx, newy):

    interp_cache = []
    for b in bathy:
        f = interp.interp2d( b['x'], b['y'], b['topog'], kind=b['interp_kind'] )
        topog = f(newx, newy)
        topog = np.where(topog>0, 0, topog)
        interp_cache.append({'x':newx,'y':newy,'topog':topog,
                             'interp_kind':b['interp_kind'],
                             'output': b['output']})

    return interp_cache

#END interpolate_bathy

def interpolate_3dfield(field, x, y, newx, newy):

    print('Interpolating!') 
    newf = np.zeros((1,field.shape[0],newy.shape[0],newx.shape[0]))
    for k in range(field.shape[0]):
       fieldAtK = field[k,:,:]
       fieldAtK.shape = (field.shape[1],field.shape[2])
       f = interp.interp2d( x, y, fieldAtK, kind='cubic' )
       newf[0,k,:,:] = f(newx, newy)

    return newf

#END interpolate_3dfield

def write_for_netcdf(bathy):

    for b in bathy:

        nlon = b['x'].size
        nlat = b['y'].size
        ncf = b['output']+'.nc'
        rootgrp = nc.Dataset(ncf,'w',format="NETCDF4")
        lon = rootgrp.createDimension('lon',nlon)
        lat = rootgrp.createDimension('lat',nlat)
        longitude = rootgrp.createVariable('lon','f4',('lon',))
        latitude = rootgrp.createVariable('lat','f4',('lat',))
        topog = rootgrp.createVariable('topog','f4',('lat','lon',))
        topog.units = 'm'

        longitude[:] = b['x']
        latitude[:] = b['y']
        topog[:,:] = b['topog']
        

        rootgrp.close()

def write_state_to_nc(state, grid, newx, newy, timelevel):

    nz = state['temp'].shape[1]
    nlat = state['temp'].shape[2]
    nlon = state['temp'].shape[3]
    
    ncf = 'state.downscaled.%03d.nc' % timelevel
    rootgrp = nc.Dataset(ncf,'w',format="NETCDF4")
    time = rootgrp.createDimension('time', None)
    lon = rootgrp.createDimension('lon',nlon)
    lat = rootgrp.createDimension('lat',nlat)
    z = rootgrp.createDimension('z',nz)
    longitude = rootgrp.createVariable('lon','f4',('lon',))
    latitude = rootgrp.createVariable('lat','f4',('lat',))
    depth = rootgrp.createVariable('z','f4',('z',))
    timeVar = rootgrp.createVariable('time', 'f4',('time',))
    temp = rootgrp.createVariable('Temp','f4',('time','z','lat','lon'))
    salt = rootgrp.createVariable('Salinity','f4',('time','z','lat','lon'))
    u = rootgrp.createVariable('U','f4',('time','z','lat','lon'))
    v = rootgrp.createVariable('V','f4',('time','z','lat','lon'))
    w = rootgrp.createVariable('W','f4',('time','z','lat','lon'))

    longitude[:] = newx[:]
    latitude[:] = newy[:]
    timeVar[:] = np.array([timelevel])[:]
    depth[:] = grid['Z'][:]
    temp[:] = state['temp'][:]
    salt[:] = state['salt'][:]
    u[:] = state['u'][:]
    v[:] = state['v'][:]
    w[:] = state['w'][:]
    
    rootgrp.close()

def state_process(time_level, config, grid, newx, newy):

        state = load_mitgcm_state(config['mitgcm_source']['state_files'][0],time_level)

        print('Filling in dry grid cells for temperature')
        state['temp'] = inpaint_fillvalues(state['temp'], grid['HFacC'])
        temp = interpolate_3dfield(state['temp'], grid['lon'], grid['lat'], newx, newy)

        print('Filling in dry grid cells for salinity')
        state['salt'] = inpaint_fillvalues(state['salt'], grid['HFacC'])
        salt = interpolate_3dfield(state['salt'], grid['lon'], grid['lat'], newx, newy)

        print('Filling in dry grid cells for zonal velocity')
        state['u'] = inpaint_fillvalues(state['u'], grid['HFacW'])
        u = interpolate_3dfield(state['u'], grid['lon'], grid['lat'], newx, newy)

        print('Filling in dry grid cells for meridional velocity')
        state['v'] = inpaint_fillvalues(state['v'], grid['HFacS'])
        v = interpolate_3dfield(state['v'], grid['lon'], grid['lat'], newx, newy)

        print('Filling in dry grid cells for vertical velocity')
        state['w'] = inpaint_fillvalues(state['w'], grid['HFacC'])
        w = interpolate_3dfield(state['w'], grid['lon'], grid['lat'], newx, newy)

        interp_state = {'salt': salt,
                        'temp': temp,
                        'u': u,
                        'v': v,
                        'w': w}

        write_state_to_nc(interp_state, grid, newx, newy, time_level)

#END state_process

def write_binary_files(config):

    state_vars = ['Temp', 'Salinity', 'U', 'V', 'W']
    for timelevel in range(config['mitgcm_source']['n_timelevels']):
        ncf = 'state.downscaled.%03d.nc' % timelevel
        ncid = nc.Dataset(ncf,'r')

        for v in state_vars:
            print(v)
            temp = ncid.variables[v][0,:,:,:]
            data = temp.flatten().data
            # > Initial condition
            if timelevel == 0:
                bytedata = struct.pack('>'+'f'*len(data), *data)
                with open(config['output_directory']+'/'+v+".init.bin","wb") as f:
                    f.write(bytedata)

            # South boundary
            data = temp[:,0,:].flatten().data
            bytedata = struct.pack('>'+'f'*len(data), *data)
            with open(config['output_directory']+'/'+v+".south.bin","ab") as f:
                f.write(bytedata)

            # North boundary
            data = temp[:,-1,:].flatten().data
            bytedata = struct.pack('>'+'f'*len(data), *data)
            with open(config['output_directory']+'/'+v+".north.bin","ab") as f:
                f.write(bytedata)

            # West boundary
            data = temp[:,:,0].flatten().data
            bytedata = struct.pack('>'+'f'*len(data), *data)
            with open(config['output_directory']+'/'+v+".west.bin","ab") as f:
                f.write(bytedata)

            # East boundary
            data = temp[:,:,-1].flatten().data
            bytedata = struct.pack('>'+'f'*len(data), *data)
            with open(config['output_directory']+'/'+v+".east.bin","ab") as f:
                f.write(bytedata)


        ncid.close()
        #temp = rootgrp.createVariable('Temp','f4',('time','z','lat','lon'))
        #salt = rootgrp.createVariable('Salinity','f4',('time','z','lat','lon'))
        #u = rootgrp.createVariable('U','f4',('time','z','lat','lon'))
        #v = rootgrp.createVariable('V','f4',('time','z','lat','lon'))
        #w = rootgrp.createVariable('W','f4',('time','z','lat','lon'))

#END write_binary_files

def create_metadata_list(config):

    # We assume that the directories are given in chronological order
    # and that each directory contains a complete year of simulation output
    files_3d = []
    files_2d = []

    files_per_year = config['mitgcm_source']['days_per_year']/config['mitgcm_source']['database_frequency_days']
    
    for d in config['mitgcm_source']['database_directories']:
        
        

#END create_metadata_list

def main():

    config = load_config()
    print(json.dumps(config,sort_keys=True,indent=2))

    newx, newy = generate_target_grid(config)

    grid = load_mitgcm_grid(config)

    if config['mitgcm_source']['format'] == 'mitgcm_netcdf':
        # For each time level...
        num_cores = config['n_cores']
        p_list = Parallel(n_jobs = num_cores)(delayed(state_process)(time_level, config, grid, newx, newy) 
                                                for time_level in range(config['mitgcm_source']['n_timelevels']))

    elif config['mitgcm_source']['format'] == 'mitgcm_metadata':

        # Create list of files to process


    write_binary_files(config)


#END main

if __name__ == '__main__' :
    main()

